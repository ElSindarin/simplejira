package com.hillel.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "org_structure")
@SequenceGenerator(name = "seq_name", sequenceName = "seq_org_structure", allocationSize = 1)
public class Structure extends BasicEntity{

    public Structure() {
    }

    @Column(name = "name")
    private String name;

    @Column (name = "cl_position")
    private String position;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "structure")
    private List<User> users = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
