package com.hillel.model;

import javax.persistence.*;
import java.sql.Blob;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "task")
@SequenceGenerator(name = "seq_name", sequenceName = "seq_task", allocationSize = 1)
public class Task extends BasicEntity{

    public Task() {
    }

    @Column (name = "name")
    private String name;

    @Column (name = "description")
    private String description;

    @Column (name = "type")
    @Enumerated (value = EnumType.STRING)
    private TaskType type;

    @ManyToOne
    @JoinColumn (name = "backlog_id")
    private Backlog backlog;

    @ManyToOne
    @JoinColumn (name = "sprint_id")
    private Sprint sprint;

//    @Column (name = "attachment")
//    private Blob attachment;

    @Column (name = "finish_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar finishTime;

    @ManyToMany
    @JoinTable (name = "task_to_client", joinColumns = {@JoinColumn (name = "task_id")}, inverseJoinColumns = {@JoinColumn (name = "client_id")})
    private Set<User> users = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public Backlog getBacklog() {
        return backlog;
    }

    public void setBacklog(Backlog backlog) {
        this.backlog = backlog;
    }

    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    public Calendar getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Calendar finishTime) {
        this.finishTime = finishTime;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
