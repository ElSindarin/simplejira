package com.hillel.model;

public enum UserType {
    USER, ADMIN
}
