package com.hillel.model;

public enum SprintStatus {
    IN_PROGRESS, COMPLETED
}
