package com.hillel.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "sprint")
@SequenceGenerator(name = "seq_name", sequenceName = "seq_sprint", allocationSize = 1)
public class Sprint extends BasicEntity {

    public Sprint() {
    }

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn (name = "backlog_id")
    private Backlog backlog;

    @Column (name = "duration")
    private Integer duration;

    @Column (name = "status")
    @Enumerated (value = EnumType.STRING)
    private SprintStatus status;

    @ManyToMany
    @JoinTable (name = "sprint_to_client", joinColumns = {@JoinColumn (name = "sprint_id")}, inverseJoinColumns = {@JoinColumn (name = "client_id")})
    private Set<User> users = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Backlog getBacklog() {
        return backlog;
    }

    public void setBacklog(Backlog backlog) {
        this.backlog = backlog;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public SprintStatus getStatus() {
        return status;
    }

    public void setStatus(SprintStatus status) {
        this.status = status;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
