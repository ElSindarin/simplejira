package com.hillel.model;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "client")
@SequenceGenerator(name = "seq_name", sequenceName = "seq_client", allocationSize = 1)
public class User extends BasicEntity{

    public User() {
    }

    @Column(name = "name")
    private String name;

    @Column (name = "type")
    @Enumerated(value = EnumType.STRING)
    private UserType userType;

    @ManyToOne
    @JoinColumn(name = "position_id")
    private Structure structure;

    @ManyToMany(mappedBy = "users")
    private Set<Sprint> sprints = new HashSet<>();

    @ManyToMany(mappedBy = "users")
    private Set<Task> tasks = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Structure getStructure() {
        return structure;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
    }

    public Set<Sprint> getSprints() {
        return sprints;
    }

    public void setSprints(Set<Sprint> sprints) {
        this.sprints = sprints;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }
}
