package com.hillel.model;

public enum TaskType {
    FEATURE, BUGFIX, SUPPORT
}
