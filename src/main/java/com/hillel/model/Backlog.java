package com.hillel.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "backlog")
@SequenceGenerator(name = "seq_name", sequenceName = "seq_backlog", allocationSize = 1)
public class Backlog extends BasicEntity {

    public Backlog() {
    }

    @Column(name = "name")
    private String name;

    @OneToOne(mappedBy = "backlog")
    private Project project;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "backlog")
    private List<Task> tasks = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "backlog")
    private List<Sprint> sprints = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Sprint> getSprints() {
        return sprints;
    }

    public void setSprints(List<Sprint> sprints) {
        this.sprints = sprints;
    }
}
