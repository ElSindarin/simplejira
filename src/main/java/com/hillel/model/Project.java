package com.hillel.model;

import javax.persistence.*;

@Entity
@Table(name = "project")
@SequenceGenerator(name = "seq_name", sequenceName = "seq_project", allocationSize = 1)
public class Project extends BasicEntity {

    public Project() {
    }

    @Column(name = "name")
    private String name;

    @Column (name = "description")
    private String description;

    // add one-to-one relationship with Backlog
    @OneToOne
    @JoinColumn(name = "backlog_id", referencedColumnName = "id")
    private Backlog backlog;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Backlog getBacklog() {
        return backlog;
    }

    public void setBacklog(Backlog backlog) {
        this.backlog = backlog;
    }
}
